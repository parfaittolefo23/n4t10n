![Contexte](images/web-1-2.png)

**Nom:** Rise from the Ashes

**Pts:** 50

Lien: http://thephoenixquest.acxsit.com:30000/

**Fisrt blood 🩸**

# Solution

Première des chose, j'ai visité la page web et je vois...

![Page web](images/web-1-3.png)

J'ai pensée aux failles XSS ou Sqli de base vu qu'il sagit d'un formulaire. Je vais commencé  par tester mes hypothèse. en commant pas **SQLi** car il me semble plus facilé à identifier qu'un **XSS**

**>> identification de la faille | SQLi**

Plusieurs payloads sont proposés par [PayloadsAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/SQL%20Injection/SQLite%20Injection.md), dans mon cas un simple **`'`** suffit

![SQLi error](images/web-1-4.png)

_Il s'agit d'un SQLi !!!_

**>> Recherche d'exploit**

Après avoir identifier la faile du système, il nous faut chercher l'exploit à fin de pwn la cible... 
Ce pouvait se fait manuellement en se réfférent à [hacktricks](https://book.hacktricks.wiki/en/pentesting-web/sql-injection/index.html) mais j'ai préfféré utilisé SQLmap.
J'ai fait ce choix pour gagner du temps car le facteur temps est important ici...

1. identifier la db

    `sqlmap http://thephoenixquest.acxsit.com:30000/index.php?search=1 --dbs`

    ![SQLi error](images/web-1-5.png)

2. Dump la db tpq_ctf

    `sqlmap http://thephoenixquest.acxsit.com:30000/index.php?search=1 --dbs  -D tpq_ctf --dump`

    ![SQLi error](images/web-1-6.png)

**FLAG: TPQCTF{uni0n_4ttack_5uccessful}**

_**N.B. : Certaines phases ont été omises, car il ne s'agissait pas d'un pentesting réel.**_