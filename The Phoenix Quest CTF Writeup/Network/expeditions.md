

![Contexte](images/net-1.png)


**Nom:** Expeditions

**Pts:** 70

**Second blood 🩸**

**File:**[Expeditions](https://gitlab.com/parfaittolefo23/n4t10n/-/blob/main/The%20Phoenix%20Quest%20CTF%20Writeup/Network/images/expeditions.pcap)

# solution

`strings expeditions.pcap`

![strings](images/net-1-1.png)

Il y a des chaines en base64. On peut les filtrées avec:

`strings expeditions.pcap | grep '"'`

![strings](images/net-1-2.png)


Et decoder from base64, on obtient le flag

![flag](images/net-1-3.png)
