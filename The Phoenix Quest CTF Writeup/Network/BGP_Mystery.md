![Contexte](images/net-2.png)

**Nom:** BGP Mystery

**Pts:** 100

**First blood 🩸**

File: [bgp](https://gitlab.com/parfaittolefo23/n4t10n/-/blob/main/The%20Phoenix%20Quest%20CTF%20Writeup/Network/images/bgp.pcapng) 

# Solution

Nous allons travailler que les requêttes **BGP** 

![wireshark](images/net-2-1.png)


**>> Réponse à la question _1- Quels sont les préfixes IP annoncés et restreints uniquement à l'AS 65100 ?_**

Il nous faut chercher dans les requêttes avec **update message** et l'AS et les préfixes

![wireshark](images/net-2-3.png)

![wireshark](images/net-2-4.png)

![wireshark](images/net-2-5.png)

Reponse_1: 10.30.0.0/16,10.40.0.0/16

**>> Réponse à la question _2 Quel AS utilise le BGP Extended Community (00:7b:01:c8:01:41:02:8e) ?_**

La Réponse est **65000**

Réf: https://www.wireshark.org/docs/dfref/b/bgp.html


FLAG: **TPQCTF{10.30.0.0/16,10.40.0.0/16_65000}**


