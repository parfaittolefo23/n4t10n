![Contexte](images/net-3.png)

**Nom:**  Here We Go !

**Pts:** 600

**File:**[Here We Go ](https://gitlab.com/parfaittolefo23/n4t10n/-/blob/main/The%20Phoenix%20Quest%20CTF%20Writeup/Network/images/Here_We_Go.pcap)

**First blood 🩸**

# SOLUTION

1. AP-name: **test-ap**

![ap-name](images/net-3-1.png)

2. AP Password

    `aircrack-ng -J handshake Here_We_Go.pcap`

    ![ap-name](images/net-3-2.png)

    `aircrack-ng -w wordlist.txt -b 64:E5:99:7A:E9:64 Here_We_Go.pcap -w /usr/share/wordlists/rockyou.txt`

    ![ap-name](images/net-3-3.png)

3. AP MAC

**64:E5:99:7A:E9:64**

Une fois que j'ai eu l'adresse MAC de l'AP et avec un nom **test-ap**, je me suis dit qu'il faut chercher l'existence de ses infos en ligne

Puis je tombe sur:  https://gilgil.gitlab.io/2020/09/23/1.html


    AP Info
        mac : 64:e5:99:7a:e9:64
        ssid : test-ap
        password : abcdefgh
        channel : 1


    Station Info
        mac : e4:f8:9c:67:e4:cc
        ip : 10.2.2.2(gateway 10.2.2.1)




FLAG: TPQCTF{test-ap_abcdefgh_64:e5:99:7a:e9:64_e4:f8:9c:67:e4:cc_10.2.2.2_10.2.2.1}